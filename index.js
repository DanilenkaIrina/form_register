const root = document.getElementById('root');
const header = document.createElement('header');
const buttonMain = document.createElement('button');
const buttonSingIn = document.createElement('button');
const buttonRegister = document.createElement('button');

const wrapperReg = document.createElement('div');
const inputLogin = document.createElement('input');
const inputPass = document.createElement('input');
const inputPassConf = document.createElement('input');
const wrappperButton = document.createElement('div');
const buttonSave = document.createElement('button');
const buttonReset = document.createElement('button');
const wrapperLogin = document.createElement('div');

const messageSingIn = document.createElement('div');
const messageError = document.createElement('div');
const message = document.createElement('p');

const buttonEnter = document.createElement('button');
const buttonSingOut = document.createElement('button');

let loginUnique = true;  //флаг уникальности логина//
let loginPassInUsers = false;  //флаг наличия логина и пароля в LocalStorage//
let isLoginlS  = false;  //флаг св-ва isLogin в localStorage//
let loginLS = false;   //флаг св-ва login в localStorage//

let users = [];

messageSingIn.classList.add('messageSingIn');
messageError.classList.add('messageError');
message.classList.add('message');

root.classList.add('root-element', 'bgMain', 'dislpayFlex');

const removeMessag = () => {
  messageSingIn.remove();
  messageError.remove();
  inputLogin.classList.remove('invalid', 'icon');
  inputPass.classList.remove('invalid', 'icon');
  inputPassConf.classList.remove('invalid', 'icon');
}

const drawMenu = () => {
  header.classList.add('header');
  buttonMain.classList.add('button');
  buttonSingIn.classList.add('button');
  buttonRegister.classList.add('button');
  root.classList.add('bgMain');

  buttonMain.innerHTML = 'Main';
  buttonSingIn.innerHTML = 'Sing in';
  buttonRegister.innerHTML = 'Register';

  header.append(buttonMain, buttonRegister, buttonSingIn);
  root.append(header);
}

drawMenu();

const clickReset = () => {
  inputLogin.value = '';
  inputPass.value = '';
  inputPassConf.value = '';

  removeMessag();
}

//Проверяю в localStorage, есть ли кто то залогиненный//
const checkIsLogin =() => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];

  users .forEach((element) => {
    if (element.isLogin === true) {
      isLoginlS  = true;
    }
    return isLoginlS ;
  })
}

//Проверяю в localStorage, есть ли такой логин//
const checkLogin =() => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];

  users.forEach((element) => {
    if (element.login === inputLogin.value) {
      loginLS  = true;
    }
    return loginLS ;
  })
}

const clickMain = () => {
  root.classList.remove('background');
  root.classList.add('bgMain');

  while (root.firstChild) {
    root.firstChild.remove();
  }

  checkIsLogin();

  if(isLoginlS ) {
    header.classList.add('header');
    buttonMain.classList.add('button');
    buttonSingOut.classList.add('button');
    root.classList.add('bgMain');

    buttonSingOut.innerHTML = 'Sing out';
    buttonMain.innerHTML = 'Main';
  
    header.append(buttonMain, buttonSingOut);
    root.append(header);
  } else {
    drawMenu();
  }
}

const clickRegister = () => {

  wrapperLogin.remove();

  wrapperReg.classList.add('wrapperReg');
  inputLogin.classList.add('input');
  inputPass.classList.add('input');
  inputPassConf.classList.add('input');
  wrappperButton.classList.add('wrappperButton');
  buttonSave.classList.add('button');
  buttonReset.classList.add('button');
  root.classList.add('background');

  inputLogin.setAttribute('type', 'text');
  inputLogin.setAttribute('placeholder', 'Enter Login');
  inputPass.setAttribute('type', 'password');
  inputPass.setAttribute('placeholder', 'Enter Password');
  inputPassConf.setAttribute('type', 'password');
  inputPassConf.setAttribute('placeholder', 'Confirm Password');

  buttonSave.innerHTML = 'Save';
  buttonReset.innerHTML = 'Reset';

  wrapperReg.append(inputLogin, inputPass, inputPassConf, wrappperButton);
  wrappperButton.append(buttonSave, buttonReset);
  root.append(wrapperReg);

  clickReset();
}

//Проверяю на совпадение inputPass и inputPassConf//
const checkPassword = () => {
  for (var i = 0; i <= inputLogin.value.length; i++) {
    if (inputPass.value.indexOf(inputPassConf.value) != 0) {
      message.innerHTML = 'Пароли не совпадают, попробуйте ввести пароль еще раз';
      messageError.append(message);
      wrapperReg.prepend(messageError);
      inputPass.classList.add('invalid', 'icon');
      inputPassConf.classList.add('invalid', 'icon');
      inputPassConf.value = '';
    }
  }
}

const clickSave = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];

  let user = {
    login: inputLogin.value, 
    password: inputPass.value,
    isLogin: false
  }

  checkLogin();

  if (loginLS === true) {
    message.innerHTML = 'Такой логин уже существует';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
    loginLS = false;
  } else if (inputLogin.value === '') {
    message.innerHTML = 'Заполните логин';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
  } else if (inputPassConf.value === '') {
    message.innerHTML = 'Повторите пароль';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputPassConf.classList.add('invalid', 'icon');
  } else {
    message.innerHTML = 'Логин и пароль успешно сохранены';
    wrapperReg.remove();
    messageSingIn.append(message);
    root.append(messageSingIn);

    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
    users = JSON.parse(localStorage.getItem('users'));
  }
}

const clickSingIn = () => {
  wrapperLogin.classList.add('wrapperLogin');
  inputLogin.classList.add('input');
  inputPass.classList.add('input');
  buttonEnter.classList.add('button');
  root.classList.add('background');

  buttonEnter.innerHTML = 'Enter';

  inputLogin.setAttribute('type', 'text');
  inputLogin.setAttribute('placeholder', 'Enter Login');
  inputPass.setAttribute('placeholder', 'Enter Password');
  inputPass.setAttribute('type', 'password');

  while (root.children[1]) {
    root.children[1].remove();
  }
  clickReset();

  wrapperLogin.append(inputLogin, inputPass, buttonEnter);
  root.append(wrapperLogin);
}

//Проверка, что человек был ранее зарегистрирован//
const isLoginPassInUsers = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];

  users.forEach((element) => {
    if ((inputLogin.value === element.login) && (inputPass.value === element.password)) {
      loginPassInUsers = true;
    }
    return loginPassInUsers;
  })
}

//Меняет значение свойства isLogin на 'true' при входе под логином//
const isSingIn = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  let singIn = false;

  users.forEach((element) => {
    if ((inputLogin.value === element.login) && (inputPass.value === element.password)) {
      singIn = true;
    }
    if(singIn) {
      element.isLogin = true;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

//Вид окна, когда выполнен вход под логином//
const windowSingIn = () => {
  while (header.children[0]) {
    header.children[0].remove();
  }
  clickMain();
  message.innerHTML = `Вы вошли как пользователь: ${inputLogin.value}`;
  messageSingIn.append(message);
  wrapperLogin.prepend(messageSingIn);

  while (wrapperLogin.children[1]) {
    wrapperLogin.children[1].remove();
  }
  root.append(wrapperLogin);
}

const clickEnter = () => {
  isLoginPassInUsers();

  if (!loginPassInUsers) {
    message.innerHTML = 'Неверный логин или пароль';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
    inputPass.classList.add('invalid', 'icon');
    inputPass.value = '';
  } else if (inputLogin.value === '') {
    message.innerHTML = 'Введите логин';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
  } else if (inputPass.value === '') {
    message.innerHTML = 'Введите пароль';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputPass.classList.add('invalid', 'icon');
  } else {
    isSingIn();
    windowSingIn();
  }
}

//Меняет значение свойства isLogin на 'false' при выходе//
const isSingOut = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  
  users.forEach((element) => {
    if(element.isLogin==true ) {
      isLoginlS  = false;
      element.isLogin = false;
      loginPassInUsers = false;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

const clickSingOut = () => {
  isSingOut();
  clickMain();
  buttonSingOut.remove();
}

const checkLoad = () => {
  checkIsLogin();
  while (header.children[0]) {
    header.children[0].remove();
  }
  clickMain();
}

window.addEventListener('load', checkLoad);
inputLogin.addEventListener('click', removeMessag);
inputPass.addEventListener('click', removeMessag);
inputPassConf.addEventListener('click', removeMessag);
buttonSingIn.addEventListener('click', clickSingIn);
buttonMain.addEventListener('click', clickMain);
buttonEnter.addEventListener('click', clickEnter);
buttonRegister.addEventListener('click', clickRegister, removeMessag);
inputPassConf.addEventListener('keyup', checkPassword);
buttonReset.addEventListener('click', clickReset);
buttonSave.addEventListener('click', clickSave);
buttonSingOut.addEventListener('click', clickSingOut);